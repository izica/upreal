import * as React from 'react';
import { browserHistory } from 'react-router';

import { SearchStore } from '../../stores/SearchStore.jsx';
import { PropertiesStore } from '../../stores/PropertiesStore.jsx';

export const IndexSearch = React.createClass({
    getInitialState() {
        return {
            dropdown: false,
            search: "",
<<<<<<< HEAD
            placeholder: "City, Neighborhood, Development, etc",
            type: 'all',
            status: 'sale',
=======
            type: 'all',
>>>>>>> origin/master
            types: {
                all: 'All',
                commercial: 'Commercial',
                residential: 'Residential'
            },
            load: false
        };
    },
    componentDidMount() {
        this.setState({
            dropdown: false,
            load: false
        });
        window.addEventListener('click', this.dropdownHide, false);
        PropertiesStore.bind(PropertiesStore.events.items.update, this.searchFinish);
        SearchStore.getData();
    },
    componentWillUnmount() {
        window.removeEventListener('click', this.dropdownHide, false);
        PropertiesStore.unbind(PropertiesStore.events.items.update, this.searchFinish);
    },
    searchFinish(){
<<<<<<< HEAD
        if(PropertiesStore.items.length==0){
            this.setState({
                search: "",
                placeholder: "0 properties was found",
                load: false
            });
        }else{
            browserHistory.push("/properties");
        }
=======
        browserHistory.push("/properties");
>>>>>>> origin/master
    },
    dropdownHide(){
        this.setState({
            dropdown: false
        });
    },
    handleSelect(type){
        this.setState({
            type: type,
            dropdown:false
        });
    },
    dropdownToggle(e){
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            dropdown: !this.state.dropdown
        });
    },
    handleSubmit(e){
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            load: true
        });
<<<<<<< HEAD
        PropertiesStore.getList(this.state.search, this.state.type, this.state.status);
=======
        PropertiesStore.getList(this.state.search);
>>>>>>> origin/master
    },
    render() {
        return (
            <form className="index-search" onSubmit={this.handleSubmit}>
                <div className="index-search__container">
                    <div className="index-search__select">
                        <div className="index-search__filter" onClick={this.dropdownToggle}>{this.state.types[this.state.type]}</div>
                        <div className="index-search__list" style={{display: (this.state.dropdown) ? "block" : "none"}}>
                            <div className="index-search__item" onClick={ (e) => this.handleSelect("all") }>All</div>
                            <div className="index-search__item" onClick={ (e) => this.handleSelect("residential") }>Residential</div>
                            <div className="index-search__item" onClick={ (e) => this.handleSelect("commercial") }>Commercial</div>
                        </div>
                    </div>
<<<<<<< HEAD
                    <div className="index-search__input">
                        <div className="index-search__tabs">
                            <div className={(this.state.status=='sale') ? "index-search__tab index-search__tab_active" : "index-search__tab"}
                                onClick={ (e) => this.setState({status: 'sale'})}>For Sale</div>
                            <div className={(this.state.status=='rent') ? "index-search__tab index-search__tab_active" : "index-search__tab"}
                                onClick={ (e) => this.setState({status: 'rent'})}>For Rent</div>
                            <div className={(this.state.status=='lease') ? "index-search__tab index-search__tab_active" : "index-search__tab"}
                                onClick={ (e) => this.setState({status: 'lease'})}>For Lease</div>
                        </div>
                        <input
                            placeholder={this.state.placeholder}
                            type="text"
                            onChange={ (e) => this.setState({search: e.target.value}) }
                            value={this.state.search}/>
                    </div>
=======
                    <input
                        placeholder="City, Neighborhood, Development, etc"
                        className="index-search__input"
                        type="text"
                        onChange={ (e) => this.setState({search: e.target.value}) }
                        value={this.state.search}/>
>>>>>>> origin/master
                    <button
                        type="submit"
                        className={(this.state.load) ? "index-search__btn index-search__btn_load" : "index-search__btn"}>
                    </button>
                </div>
            </form>
        );
    }
})
